/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package libvirt

import (
	"fmt"

	"github.com/libvirt/libvirt-go"
)

// Libvirt is the responsible for interacting with the Libvirt in the hypervisor
type Libvirt struct {
	Host string
	conn *libvirt.Connect
}

// Connect starts the connection with the libvirt of the hypervisor
func (l *Libvirt) Connect() error {
	var err error
	l.conn, err = libvirt.NewConnect(fmt.Sprintf("qemu+ssh://root@%s:22/system", l.Host))
	if err != nil {
		return fmt.Errorf("error connecting to the hypervisor libvirt: %v", err)
	}

	return nil
}

func (l *Libvirt) GetDomainIP(id string) (string, error) {
	return "", nil
}
