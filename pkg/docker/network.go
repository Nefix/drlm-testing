/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package docker

import (
	"fmt"
	"os/exec"
	"strings"
)

// GetContainerIPs returns all the IP addresses that a container has
func GetContainerIPs(name string) ([]string, error) {
	cmd := exec.Command("docker", "inspect", name, "-f", "{{ range .NetworkSettings.Networks }}{{ .IPAddress }}{{end}}")
	out, err := cmd.Output()
	if err != nil {
		return []string{}, fmt.Errorf("error getting %s IP addresses: %v", name, err)
	}

	return strings.Split(string(out), "\n"), nil
}
