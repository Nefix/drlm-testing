/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package docker

import (
	"fmt"
	"os/exec"
)

// Pull downloads the latest docker images from DockerHub
func Pull() error {
	cmd := exec.Command("docker-compose", "pull")
	if out, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("error pulling the latest images from DockerHub: %v: %s", err, out)
	}

	return nil
}

// Up starts IsardVDI
func Up() error {
	cmd := exec.Command("docker-compose", "up", "-d")
	if out, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("error starting IsardVDI: %v: %s", err, out)
	}

	return nil
}

// Down stops IsardVDI
func Down() error {
	cmd := exec.Command("docker-compose", "down")
	if out, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("error stopping IsardVDI: %v: %s", err, out)
	}

	return nil
}
