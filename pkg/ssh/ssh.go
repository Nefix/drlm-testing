/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ssh

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/ssh"
)

// SSH is the responsible for executing commands through SSH
type SSH struct {
	Host string
	Port int

	conn *ssh.Client
}

// Connect starts the SSH connection
func (s *SSH) Connect(config *ssh.ClientConfig) error {
	var err error
	s.conn, err = ssh.Dial("tcp", fmt.Sprintf("%s:%d", s.Host, s.Port), config)
	if err != nil {
		return fmt.Errorf("error dialing the tunneled connection: %v", err)
	}

	return nil
}

// newSession creates a new session
func (s *SSH) newSession() (*ssh.Session, error) {
	sess, err := s.conn.NewSession()
	if err != nil {
		return sess, fmt.Errorf("error creating the SSH session: %v", err)
	}

	modes := ssh.TerminalModes{
		ssh.ECHO:          0,
		ssh.TTY_OP_ISPEED: 14400,
		ssh.TTY_OP_OSPEED: 14400,
	}

	if err := sess.RequestPty("xterm", 80, 40, modes); err != nil {
		return sess, fmt.Errorf("error requesting the PTY: %v", err)
	}

	return sess, nil
}

// Run executes a command and returns the output
func (s *SSH) Run(cmd string) (string, error) {
	sess, err := s.newSession()
	if err != nil {
		return "", fmt.Errorf("error creating the SSH session: %v", err)
	}
	defer sess.Close()

	out, err := sess.CombinedOutput(cmd)

	return string(out), err
}

// extracted from SSH pacakge
type singleWriter struct {
	b  bytes.Buffer
	mu sync.Mutex
}

func (w *singleWriter) Write(p []byte) (int, error) {
	w.mu.Lock()
	defer w.mu.Unlock()
	return w.b.Write(p)
}

type doubleWriter struct {
	b           bytes.Buffer
	mu          sync.Mutex
	ExtraWriter *singleWriter
}

func (w *doubleWriter) Write(p []byte) (int, error) {
	if num, err := w.ExtraWriter.Write(p); err != nil {
		return num, err
	}

	w.mu.Lock()
	defer w.mu.Unlock()
	return w.b.Write(p)
}

// Stdin contains the time before stdin is going to be sent through SSH and the content that is going to be sent
type Stdin struct {
	Sleep time.Duration
	Stdin string
}

// RunWithStdin executes a command with stdin and returns the output
// sleep is the amount of seconds that the command is going to wait between each stdin write
func (s *SSH) RunWithStdin(cmd string, in ...Stdin) (string, error) {
	sess, err := s.newSession()
	if err != nil {
		return "", fmt.Errorf("error creating the SSH session: %v", err)
	}
	defer sess.Close()

	stdin, err := sess.StdinPipe()
	if err != nil {
		return "", fmt.Errorf("error piping stdin: %v", err)
	}

	var stderr singleWriter
	sess.Stderr = &stderr

	var stdout = doubleWriter{
		ExtraWriter: &stderr,
	}
	sess.Stdout = &stdout

	if err := sess.Start(cmd); err != nil {
		return "", fmt.Errorf("error executing the command: %v", err)
	}

	for _, p := range in {
		time.Sleep(p.Sleep)
		p.Stdin += "\n"
		stdin.Write([]byte(p.Stdin))
		fmt.Println(p.Stdin)
	}

	err = sess.Wait()
	if err != nil {
		return stderr.b.String(), err
	}

	return stdout.b.String(), nil
}

// Tunnel is a SSH tunnel through different servers
type Tunnel struct {
	Local  *Endpoint
	Server *Endpoint
	Remote *Endpoint

	Config *ssh.ClientConfig
}

// Start starts the SSH Tunnel
func (t *Tunnel) Start(quit *chan struct{}) error {
	lis, err := net.Listen("tcp", t.Local.String())
	if err != nil {
		return fmt.Errorf("error listening to the local address: %v", err)
	}
	defer lis.Close()

	for {
		select {
		case <-*quit:
			return nil

		default:
			conn, err := lis.Accept()
			if err != nil {
				return fmt.Errorf("error accepting the connection: %v", err)
			}

			go t.forward(conn)
		}
	}
}

// forward forwards a connection to the remote server
func (t *Tunnel) forward(localConn net.Conn) {
	serverConn, err := ssh.Dial("tcp", t.Server.String(), t.Config)
	if err != nil {
		fmt.Printf("error dialing the server: %v", err)
		return
	}

	log.Info(t.Remote.String())
	remoteConn, err := serverConn.Dial("tcp", t.Remote.String())
	if err != nil {
		fmt.Printf("error dialing the remote: %v", err)
		return
	}

	copyConn := func(writer, reader net.Conn) {
		_, err := io.Copy(writer, reader)
		if err != nil {
			fmt.Printf("error copying io.Copy: %v", err)
		}
	}

	go copyConn(localConn, remoteConn)
	go copyConn(remoteConn, localConn)
}

// Endpoint is a server in a SSH tunnel
type Endpoint struct {
	// Server address
	Host string

	// Server port
	Port int
}

func (e *Endpoint) String() string {
	return fmt.Sprintf("%s:%d", e.Host, e.Port)
}
