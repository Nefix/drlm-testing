/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package isard

import (
	"fmt"
	"path/filepath"
	"time"

	"github.com/tebeka/selenium"
)

// toBeDisplayed waits until an element is displayed
func (i *Isard) toBeDisplayed(by, selector string) (el selenium.WebElement, err error) {
	i.wd.Wait(func(wd selenium.WebDriver) (bool, error) {
		el, err = i.wd.FindElement(by, selector)
		if err != nil {
			return false, nil
		}

		displayed, err := el.IsDisplayed()
		return displayed, err
	})

	return el, err
}

// toBeClickable waits until an element is clickable
func (i *Isard) toBeClickable(by, selector string) (el selenium.WebElement, err error) {
	el, err = i.toBeDisplayed(by, selector)
	if err != nil {
		return el, err
	}

	i.wd.Wait(func(wd selenium.WebDriver) (bool, error) {
		enabled, err := el.IsEnabled()
		return enabled, err
	})

	return el, err
}

// toHaveText waits until an element has a specific text
func (i *Isard) toHaveText(by, selector, text string) (el selenium.WebElement, err error) {
	el, err = i.toBeDisplayed(by, selector)
	if err != nil {
		return el, err
	}

	i.wd.Wait(func(wd selenium.WebDriver) (bool, error) {
		elText, err := el.Text()
		if err != nil {
			return false, err
		}

		if elText != text {
			return false, nil
		}

		return true, nil
	})

	return el, err
}

// navigateTo changes the Selenium page to the specified as a parameter
func (i *Isard) navigateTo(path string) error {
	return i.wd.Get(fmt.Sprintf("https://%s%s", i.Host, path))
}

// ExecuteWizard setups Isard using the Wizard
func (i *Isard) ExecuteWizard() error {
	// Start
	updatesCheck, err := i.toBeClickable(selenium.ByCSSSelector, "#reg")
	if err != nil {
		return err
	}
	if err = updatesCheck.Click(); err != nil {
		return err
	}

	startBtn, err := i.toBeClickable(selenium.ByCSSSelector, "#send-reg")
	if err != nil {
		return err
	}
	if err = startBtn.Click(); err != nil {
		return err
	}
	time.Sleep(1 * time.Second)

	// 1- Configuration
	if _, err = i.toHaveText(selenium.ByCSSSelector, "#step-1 h2.StepTitle", "Step 1. Configuration"); err != nil {
		return err
	}

	nextBtn, err := i.toBeClickable(selenium.ByCSSSelector, ".buttonNext")
	if err != nil {
		return err
	}
	if err = nextBtn.Click(); err != nil {
		return err
	}

	// 2- Database
	if _, err = i.toHaveText(selenium.ByCSSSelector, "#step-2 h2.StepTitle", "Step 2. Rethinkdb database service and isard database."); err != nil {
		return err
	}

	populateBtn, err := i.toBeClickable(selenium.ByXPATH, "//a[button[text() = 'Populate isard database!']]")
	if err != nil {
		return err
	}
	if err = populateBtn.Click(); err != nil {
		return err
	}

	time.Sleep(15 * time.Second)
	if err = nextBtn.Click(); err != nil {
		return err
	}

	// 3- Password
	if _, err = i.toHaveText(selenium.ByCSSSelector, "#step-3 h2.StepTitle", "Step 3. Change default admin password"); err != nil {
		return err
	}

	pwdBtn, err := i.toBeClickable(selenium.ByXPATH, "//button[text() = 'Change password']")
	if err != nil {
		return err
	}
	if err = pwdBtn.Click(); err != nil {
		return err
	}

	pwdInput1, err := i.toBeClickable(selenium.ByCSSSelector, "#password1")
	if err != nil {
		return err
	}
	if err = pwdInput1.SendKeys(i.Password); err != nil {
		return err
	}

	pwdInput2, err := i.toBeClickable(selenium.ByCSSSelector, "#password2")
	if err != nil {
		return err
	}
	if err = pwdInput2.SendKeys(i.Password); err != nil {
		return err
	}

	updatePwdBtn, err := i.toBeClickable(selenium.ByXPATH, "//button[text() = 'Update']")
	if err != nil {
		return err
	}
	if err = updatePwdBtn.Click(); err != nil {
		return err
	}

	time.Sleep(1 * time.Second)
	if err = nextBtn.Click(); err != nil {
		return err
	}

	// 4- Engine
	if _, err = i.toHaveText(selenium.ByCSSSelector, "#step-4 h2.StepTitle", "Step 4. Isard Engine"); err != nil {
		return err
	}

	if err = nextBtn.Click(); err != nil {
		return err
	}

	// 5- Hypervisor
	if _, err = i.toHaveText(selenium.ByCSSSelector, "#step-5 h2.StepTitle", "Step 5. Hypervisors"); err != nil {
		return err
	}

	if err = nextBtn.Click(); err != nil {
		return err
	}

	// 6- Finish
	if _, err = i.toHaveText(selenium.ByCSSSelector, "#step-6 h2.StepTitle", "Step 6. Updates"); err != nil {
		return err
	}

	finishBtn, err := i.toBeClickable(selenium.ByCSSSelector, ".buttonFinish")
	if err != nil {
		return err
	}
	return finishBtn.Click()
}

// Login logs in as admin using the password used when creating isard [isard.New("")]
func (i *Isard) Login() error {
	usrInput, err := i.toBeClickable(selenium.ByName, "user")
	if err != nil {
		return err
	}
	if err = usrInput.SendKeys("admin"); err != nil {
		return err
	}

	pwdInput, err := i.toBeClickable(selenium.ByName, "password")
	if err != nil {
		return err
	}
	if err = pwdInput.SendKeys(i.Password); err != nil {
		return err
	}

	loginBtn, err := i.toBeClickable(selenium.ByTagName, "button")
	if err != nil {
		return nil
	}
	return loginBtn.Click()
}

// UploadDBBackup uploads a previously taken DB Backup to Isard
func (i *Isard) UploadDBBackup(path string) error {
	if err := i.navigateTo("/admin/config"); err != nil {
		return err
	}

	uploadBtn, err := i.toBeClickable(selenium.ByCSSSelector, ".btn-backups-upload")
	if err != nil {
		return err
	}
	if err = uploadBtn.Click(); err != nil {
		return err
	}

	uploadInput, err := i.toBeClickable(selenium.ByXPATH, "//form[@action = '/admin/backup/upload']")
	if err != nil {
		return err
	}

	// https://stackoverflow.com/questions/34761241/selenium-webdriver-upload-file-by-drag-and-drop
	const dropFileScript string = "var tgt=arguments[0],e=document.createElement('input');e.type='file';e.setAttribute('id', 'upload-backup-dragndrop-input');e.addEventListener('change',function(event){var dataTransfer={dropEffect:'',effectAllowed:'all',files:e.files,items:{},types:[],setData:function(format,data){},getData:function(format){}};var emit=function(event,target){var evt=document.createEvent('Event');evt.initEvent(event,true,false);evt.dataTransfer=dataTransfer;target.dispatchEvent(evt);};emit('dragenter',tgt);emit('dragover',tgt);emit('drop',tgt);document.body.removeChild(e);},false);document.body.appendChild(e);return e;"
	path, err = filepath.Abs(path)
	if err != nil {
		return fmt.Errorf("error getting the absoulute path of the backup: %v", err)
	}

	_, err = i.wd.ExecuteScript(dropFileScript, []interface{}{uploadInput})
	if err != nil {
		return err
	}

	scriptInput, err := i.wd.FindElement(selenium.ByCSSSelector, "#upload-backup-dragndrop-input")
	if err != nil {
		return err
	}
	if err = scriptInput.SendKeys(path); err != nil {
		return err
	}

	time.Sleep(2 * time.Second)

	return nil
}

// RestoreDBBackup restores an already imported DB backup
func (i *Isard) RestoreDBBackup(id string) error {
	if err := i.navigateTo("/admin/config"); err != nil {
		return err
	}

	restoreBtn, err := i.toBeClickable(selenium.ByXPATH, fmt.Sprintf("//tr[@id = '%s']//button[@id = 'btn-backups-restore']", id))
	if err != nil {
		return err
	}
	if err = restoreBtn.Click(); err != nil {
		return err
	}

	confirmBtn, err := i.toBeClickable(selenium.ByCSSSelector, ".ui-pnotify-action-button")
	if err != nil {
		return err
	}
	return confirmBtn.Click()
}

// CreateDesktop creates a new desktop
func (i *Isard) CreateDesktop(d Desktop) error {
	if err := i.navigateTo("/desktops"); err != nil {
		return err
	}

	newDesktopBtn, err := i.toBeClickable(selenium.ByCSSSelector, ".btn-new")
	if err != nil {
		return err
	}
	if err = newDesktopBtn.Click(); err != nil {
		return err
	}

	nameInput, err := i.toBeClickable(selenium.ByXPATH, "//input[@placeholder = 'New desktop name']")
	if err != nil {
		return err
	}
	if err = nameInput.SendKeys(d.Name); err != nil {
		return err
	}

	templateSearchInput, err := i.toBeClickable(selenium.ByXPATH, "//input[@placeholder = 'Search Name']")
	if err != nil {
		return err
	}
	if err = templateSearchInput.SendKeys(d.Template.Search); err != nil {
		return err
	}

	templateSelect, err := i.toBeClickable(selenium.ByXPATH, fmt.Sprintf("//div[@id = 'modalAddDesktop']//tr[@id = '%s']/td[1]", d.Template.ID))
	if err != nil {
		return err
	}
	if err = templateSelect.Click(); err != nil {
		return err
	}

	const setInputValueScript = "document.getElementById(arguments[0]).value = arguments[1].toString()"
	_, err = i.wd.ExecuteScript(setInputValueScript, []interface{}{"hardware-vcpus", d.Hardware.CPUs})
	if err != nil {
		return err
	}

	_, err = i.wd.ExecuteScript(setInputValueScript, []interface{}{"hardware-memory", d.Hardware.RAM})
	if err != nil {
		return err
	}

	createDesktopBtn, err := i.toBeClickable(selenium.ByCSSSelector, "#send")
	if err != nil {
		return err
	}
	if err = createDesktopBtn.Click(); err != nil {
		return err
	}

	time.Sleep(2 * time.Second)
	return nil
}

// StartDesktop starts a desktop
func (i *Isard) StartDesktop(d Desktop) error {
	if err := i.navigateTo("/desktops"); err != nil {
		return err
	}

	id := d.TransformNameToID()

	startBtn, err := i.toBeClickable(selenium.ByXPATH, fmt.Sprintf("//table[@id='desktops']/.//tr[@id='%s']/.//button[@id='btn-play']", id))
	if err != nil {
		return err
	}

	if err = startBtn.Click(); err != nil {
		return err
	}

	time.Sleep(5 * time.Second)
	return nil
}

// StopDesktop stops a desktop. If it's not running, it doesn't do anything
func (i *Isard) StopDesktop(d Desktop) error {
	if err := i.navigateTo("/desktops"); err != nil {
		return err
	}

	id := d.TransformNameToID()

	stopBtn, err := i.wd.FindElement(selenium.ByXPATH, fmt.Sprintf("//table[@id='desktops']/.//tr[@id='%s']/.//button[@id='btn-stop']", id))
	if err != nil {
		return err
	}

	displayed, err := stopBtn.IsDisplayed()
	if err != nil {
		return err
	}
	if !displayed {
		return nil
	}

	if err = stopBtn.Click(); err != nil {
		return err
	}

	confirmBtn, err := i.toBeClickable(selenium.ByXPATH, "//div[@role='alert']/.//button[contains(text(),'Ok')]")
	if err != nil {
		return err
	}
	if err = confirmBtn.Click(); err != nil {
		return err
	}

	time.Sleep(2 * time.Second)
	return nil
}

// RemoveAllDesktops stops and removes all the desktops
func (i *Isard) RemoveAllDesktops() error {
	if err := i.navigateTo("/admin/domains/render/Desktops"); err != nil {
		return err
	}

	selectOptionScript := func(action string) string {
		return fmt.Sprintf("$('#mactions').val('%s').trigger('change');", action)
	}

	_, err := i.wd.ExecuteScript(selectOptionScript("force_stopped"), []interface{}{})
	if err != nil {
		return err
	}

	confirmBtn, err := i.toBeClickable(selenium.ByXPATH, "//div[@role='alert']/.//button[contains(text(),'Ok')]")
	if err != nil {
		return err
	}
	if err = confirmBtn.Click(); err != nil {
		return err
	}

	_, err = i.wd.ExecuteScript(selectOptionScript("delete"), []interface{}{})
	if err != nil {
		return err
	}

	confirmBtn, err = i.toBeClickable(selenium.ByXPATH, "//div[@role='alert']/.//button[contains(text(),'Ok')]")
	if err != nil {
		return err
	}
	if err = confirmBtn.Click(); err != nil {
		return err
	}


	time.Sleep(4 * time.Second)

	return nil
}
