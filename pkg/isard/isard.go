/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package isard

import (
	"fmt"
	"os/exec"
	"os/user"
	"strings"

	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/docker"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/libvirt"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/firefox"
)

// Isard is the struct responsible for interacting with IsardVDI
type Isard struct {
	service *selenium.Service
	wd      selenium.WebDriver

	// Host is the server where the interface of Isard is run
	Host string

	// Password is the administrator password
	Password string

	// HyperIP is the IP address of the hypervisor
	HyperIP string

	Libvirt *libvirt.Libvirt
}

// New initializes the Isard structure and the webdriver
func New(host, password string) (*Isard, error) {
	ips, err := docker.GetContainerIPs("isard-hypervisor")
	if err != nil {
		return &Isard{}, err
	}

	cmd := exec.Command("which", "geckodriver")
	out, err := cmd.Output()
	if err != nil {
		return &Isard{}, fmt.Errorf("error getting the geckodriver path: %v", err)
	}

	geckoDriver := strings.Replace(string(out), "\n", "", -1)
	if geckoDriver == "geckodriver not found" {
		geckoDriver = "./vendor/geckodriver"
	}

	i := &Isard{
		Host:     host,
		Password: password,
		HyperIP:  ips[0],
	}
	opts := []selenium.ServiceOption{}

	i.service, err = selenium.NewGeckoDriverService(geckoDriver, 9999, opts...)
	if err != nil {
		return &Isard{}, fmt.Errorf("error starting the Selenium service: %v", err)
	}

	caps := selenium.Capabilities{
		"browserName":         "firefox",
		"acceptInsecureCerts": true,
	}
	caps.AddFirefox(firefox.Capabilities{
		// Args: []string{"--headless"},
	})

	i.wd, err = selenium.NewRemote(caps, "http://localhost:9999")
	if err != nil {
		return &Isard{}, fmt.Errorf("error starting the WebDriver: %v", err)
	}

	if err := i.wd.Get("https://localhost"); err != nil {
		return &Isard{}, fmt.Errorf("error accessing the Isard webpage: %v", err)
	}

	return i, nil
}

// Stop stops the webdriver
func (i *Isard) Stop() error {
	if err := i.wd.Quit(); err != nil {
		return fmt.Errorf("error stopping the WebDriver: %v", err)
	}

	if err := i.service.Stop(); err != nil {
		return fmt.Errorf("error stopping the Selenium service: %v", err)
	}

	return nil
}

// ImportBackup imports a Isard backup
func (i *Isard) ImportBackup(source string) error {
	const backupName string = "isard_backup_20190228-110424"
	const backupPath string = "./isard/" + backupName + ".tar.gz"

	cmd := exec.Command("sudo", "tar", "xzvf", source, "-C", "./isard")
	if out, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("error importing backup: error extracting the backup: %v: %s", err, out)
	}

	cmd = exec.Command("sudo", "mv", "./isard/backup.tar.gz", backupPath)
	if out, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("error importing backup: error renaming the backup: %v: %s", err, out)
	}

	usr, err := user.Current()
	if err != nil {
		return fmt.Errorf("error importing backup: error getting username: %v", err)
	}

	cmd = exec.Command("sudo", "chown", usr.Username, backupPath)
	if out, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("error importing backup: error changing backup owner: %v: %s", err, out)
	}

	if err := i.UploadDBBackup(backupPath); err != nil {
		return fmt.Errorf("error importing backup: error uploading the DB backup: %v", err)
	}

	if err := i.RestoreDBBackup(backupName); err != nil {
		return fmt.Errorf("error restoring backup: %v", err)
	}

	return nil
}

// Desktop is the information required for creating a desktop
type Desktop struct {
	Name     string
	Template Template
	Hardware Hardware
}

// TransformNameToID transforms a desktop name to the ID name of it
func (d Desktop) TransformNameToID() string {
	return fmt.Sprintf("_admin_%s", strings.Replace(d.Name, " ", "_", -1))
}

// Template is the information required for selecting a template
type Template struct {
	Search string
	ID     string
}

// Hardware is the hardware that the desktop is going to have (if empty, it's going to use the defaults)
type Hardware struct {
	CPUs int
	RAM  int
}
