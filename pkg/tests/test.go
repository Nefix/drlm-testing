/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tests

import (
	"fmt"

	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/isard"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/ssh"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/utils"

	log "github.com/sirupsen/logrus"
	stdSSH "golang.org/x/crypto/ssh"
)

// TestInterface is an interface that all the tests should implement
type TestInterface interface {
	ID() string
	Init([]interface{})
	Run(*isard.Isard) error
}

// Test is an individual test
type Test struct {
	Name         string
	Description  string
	Desktops     []*Desktop
	PreCommands  []Command
	Commands     []Command
	PostCommands []Command
}

// ID generates an ID for the test
func (t *Test) ID() string {
	dNames := []string{}

	for _, d := range t.Desktops {
		dNames = append(dNames, d.Name)
	}

	return fmt.Sprintf("%s_%v", t.Name, dNames)
}

// Init is the function responsible for preparing the test
func (t *Test) Init([]interface{}) {
	log.Panicf("Test %s isn't implemented", t.Name)
}

// Run executes the test
func (t *Test) Run(i *isard.Isard) error {
	if err := i.RemoveAllDesktops(); err != nil {
		return fmt.Errorf("error preparing the test: error removing all the desktops: %v", err)
	}

	for _, d := range t.Desktops {
		if err := i.CreateDesktop(d.Desktop); err != nil {
			return fmt.Errorf("error preparing the test: error creating the desktop %s: %v", d.Name, err)
		}

		if err := i.StartDesktop(d.Desktop); err != nil {
			return fmt.Errorf("error preparing the test: error starting the desktop: %s: %v", d.Name, err)
		}

		if err := d.SSHConn(i); err != nil {
			return fmt.Errorf("error connecting to %s through SSH: %v", d.Name, err)
		}
	}

	// DO TESTS
	log.Infof("executing test: %s", t.ID())

	for _, d := range t.Desktops {
		// Close the SSH connection
		close(*d.QuitSSH)

		if err := i.StopDesktop(d.Desktop); err != nil {
			return fmt.Errorf("error stopping the desktop %s: %v", d.Name, err)
		}

		// TODO: Make a template
	}

	return nil
}

// Desktop is a Desktop in a test
type Desktop struct {
	isard.Desktop
	IP       string
	SSHPort  int
	User     string
	Password string
	Conn     *ssh.SSH
	QuitSSH  *chan struct{}
}

// getIP gets the IP address of the desktop
func (d *Desktop) getIP(i *isard.Isard) error {
	ip, err := i.Libvirt.GetDomainIP(d.TransformNameToID())
	if err != nil {
		return err
	}

	d.IP = ip
	return nil
}

// SSHConn starts the SSH connection with the desktop
func (d *Desktop) SSHConn(i *isard.Isard) error {
	if err := d.getIP(i); err != nil {
		return fmt.Errorf("error getting the desktop IP: %v", err)
	}

	localPort, err := utils.GetFreePort()
	if err != nil {
		return fmt.Errorf("error getting a free port: %v", err)
	}

	tun := &ssh.Tunnel{
		Local: &ssh.Endpoint{
			Host: "localhost",
			Port: localPort,
		},
		Server: &ssh.Endpoint{
			Host: i.HyperIP,
			Port: 22,
		},
		Remote: &ssh.Endpoint{
			Host: d.IP,
			Port: d.SSHPort,
		},
		Config: &stdSSH.ClientConfig{
			User: d.User,
			Auth: []stdSSH.AuthMethod{
				stdSSH.Password("isard"),
			},
			HostKeyCallback: stdSSH.InsecureIgnoreHostKey(),
		},
	}

	d.QuitSSH = new(chan struct{})

	go func() {
		tun.Start(d.QuitSSH)
	}()

	d.Conn = &ssh.SSH{
		Host: "localhost",
		Port: localPort,
	}

	desktopCfg := &stdSSH.ClientConfig{
		User: d.User,
		Auth: []stdSSH.AuthMethod{
			stdSSH.Password(d.Password),
		},
		HostKeyCallback: stdSSH.InsecureIgnoreHostKey(),
	}

	if err := d.Conn.Connect(desktopCfg); err != nil {
		return err
	}

	return nil
}
