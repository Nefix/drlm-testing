/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tests

import (
	"fmt"
	"time"

	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/isard"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/ssh"

	log "github.com/sirupsen/logrus"
)

// TestResult is the result of an individual test
type TestResult struct {
	ID       string
	Passed   bool
	Notes    string
	Duration float64
}

// Summary is the summary of the test executions
type Summary struct {
	Duration float64
	Total    int
	Passed   int
	Failed   int
}

// Result is the result of the tests
type Result struct {
	Summary Summary
	Results []TestResult
}

// Run executes the tests
func Run(i *isard.Isard, tests []TestInterface) Result {
	result := Result{
		Summary: Summary{
			Total: len(tests),
		},
	}

	testsPassed := 0
	testsFailed := 0

	start := time.Now()

	// TODO: Templates cleanup

	for _, t := range tests {
		log.Infof("starting %s", t.ID())
		startTest := time.Now()

		err := t.Run(i)

		tDuration := time.Since(startTest)
		tResult := TestResult{
			ID:       t.ID(),
			Duration: tDuration.Seconds(),
		}

		if err != nil {
			tResult.Passed = false
			tResult.Notes = err.Error()
			testsFailed++
		} else {
			tResult.Passed = true
			testsPassed++
		}

		result.Results = append(result.Results, tResult)
	}

	testsDuration := time.Since(start)
	result.Summary.Duration = testsDuration.Seconds()
	result.Summary.Passed = testsPassed
	result.Summary.Failed = testsFailed

	return result
}

// Command is a command that is going to be run
type Command interface {
	Run() error
}

// SSHCommand runs a command through an SSH tunnel
type SSHCommand struct {
	Conn *ssh.SSH
	CMD  string
}

// Run executes the command
func (s *SSHCommand) Run() error {
	if out, err := s.Conn.Run(s.CMD); err != nil {
		return fmt.Errorf("error executing %s: %v: %s", s.CMD, err, out)
	}
	return nil
}

// SSHWithStdinCommand runs a command through an SSH tunnel with stdin
type SSHWithStdinCommand struct {
	Conn *ssh.SSH
	CMD  string
	In   []ssh.Stdin
}

// Run executes the command
func (s *SSHWithStdinCommand) Run() error {
	if out, err := s.Conn.RunWithStdin(s.CMD, s.In...); err != nil {
		return fmt.Errorf("error executing %s: %v: %s", s.CMD, err, out)
	}
	return nil
}
