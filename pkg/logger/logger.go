package logger

import (
	"io"
	"log"
	"os"

	"github.com/sirupsen/logrus"
)

const logFile string = "drlm-testing.log"

// Init initializes the logging
func Init() {
	f, err := os.OpenFile(logFile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalf("error initializing logs: error opening log file: %v", err)
	}

	formatter := &logrus.TextFormatter{
		FullTimestamp: true,
	}
	logrus.SetFormatter(formatter)

	logrus.SetLevel(logrus.DebugLevel)

	mw := io.MultiWriter(os.Stdout, f)
	logrus.SetOutput(mw)
}
