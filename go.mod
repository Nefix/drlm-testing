module gitea.nefixestrada.com/nefix/drlm-testing

go 1.12

require (
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/libvirt/libvirt-go v5.2.0+incompatible
	github.com/sirupsen/logrus v1.4.1
	github.com/tebeka/selenium v0.9.4-0.20181011202039-edf31bb7fd71
	golang.org/x/crypto v0.0.0-20190422183909-d864b10871cd
)
