# file generated from go.mod using vgo2nix (https://github.com/adisbladis/vgo2nix)
[
  {
    goPackagePath = "github.com/blang/semver";
    fetch = {
      type = "git";
      url = "https://github.com/blang/semver";
      rev = "v3.5.1";
      sha256 = "13ws259bwcibkclbr82ilhk6zadm63kxklxhk12wayklj8ghhsmy";
    };
  }
  {
    goPackagePath = "github.com/davecgh/go-spew";
    fetch = {
      type = "git";
      url = "https://github.com/davecgh/go-spew";
      rev = "v1.1.1";
      sha256 = "0hka6hmyvp701adzag2g26cxdj47g21x6jz4sc6jjz1mn59d474y";
    };
  }
  {
    goPackagePath = "github.com/konsorten/go-windows-terminal-sequences";
    fetch = {
      type = "git";
      url = "https://github.com/konsorten/go-windows-terminal-sequences";
      rev = "v1.0.1";
      sha256 = "1lchgf27n276vma6iyxa0v1xds68n2g8lih5lavqnx5x6q5pw2ip";
    };
  }
  {
    goPackagePath = "github.com/libvirt/libvirt-go";
    fetch = {
      type = "git";
      url = "https://github.com/libvirt/libvirt-go";
      rev = "v5.2.0";
      sha256 = "1asgzd9phbi6w4x2jiqrkgmglgswg068h8ssdyy0r97ibz6j2wh0";
    };
  }
  {
    goPackagePath = "github.com/pmezard/go-difflib";
    fetch = {
      type = "git";
      url = "https://github.com/pmezard/go-difflib";
      rev = "v1.0.0";
      sha256 = "0c1cn55m4rypmscgf0rrb88pn58j3ysvc2d0432dp3c6fqg6cnzw";
    };
  }
  {
    goPackagePath = "github.com/sirupsen/logrus";
    fetch = {
      type = "git";
      url = "https://github.com/sirupsen/logrus";
      rev = "v1.4.1";
      sha256 = "1m7ny9jkb98cxqhsp13xa5hnqh1s9f25x04q6arsala4zswsw33c";
    };
  }
  {
    goPackagePath = "github.com/stretchr/objx";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/objx";
      rev = "v0.1.1";
      sha256 = "0iph0qmpyqg4kwv8jsx6a56a7hhqq8swrazv40ycxk9rzr0s8yls";
    };
  }
  {
    goPackagePath = "github.com/stretchr/testify";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/testify";
      rev = "v1.2.2";
      sha256 = "0dlszlshlxbmmfxj5hlwgv3r22x0y1af45gn1vd198nvvs3pnvfs";
    };
  }
  {
    goPackagePath = "github.com/tebeka/selenium";
    fetch = {
      type = "git";
      url = "https://github.com/tebeka/selenium";
      rev = "edf31bb7fd71";
      sha256 = "0l7akndww2gfqslsdcgiv5hdvyiddwa2ylz1f194dpavvn78xc93";
    };
  }
  {
    goPackagePath = "golang.org/x/crypto";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/crypto";
      rev = "d864b10871cd";
      sha256 = "06k3nnfc5rqzwl59n2fbljmkz1d9d3rc5v617km6m92s3vvaj6dd";
    };
  }
  {
    goPackagePath = "golang.org/x/net";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/net";
      rev = "eb5bcb51f2a3";
      sha256 = "17k4g8krxbl84gzcs275b7gsh66dzm15fdxivjnx9xz8q84l4kby";
    };
  }
  {
    goPackagePath = "golang.org/x/sys";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sys";
      rev = "97732733099d";
      sha256 = "118hkp01i4z1f5h6hcjm0ff2ngqhrzj1f7731n0kw8dr6hvbx0sw";
    };
  }
  {
    goPackagePath = "golang.org/x/text";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/text";
      rev = "v0.3.0";
      sha256 = "0r6x6zjzhr8ksqlpiwm5gdd7s209kwk5p4lw54xjvz10cs3qlq19";
    };
  }
]
