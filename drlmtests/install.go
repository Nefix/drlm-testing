/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package drlmtests

import (
	"fmt"

	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/isard"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/tests"
)

var debFile = "drlm_2.3.0_all.deb"

// Install tests the installation of DRLM
type Install struct {
	tests.Test
}

// Init initializes the test
func (i *Install) Init(args []interface{}) {
	serverOS := args[0].(string)

	i.Name = "Install"
	i.Description = "Install tests the installation of DRLM"

	drlmServer := &tests.Desktop{
		Desktop: isard.Desktop{
			Name: "drlm-server",
			Template: isard.Template{
				ID:     Templates[serverOS].ID,
				Search: Templates[serverOS].Search,
			},
		},
		User:     "root",
		Password: "pirineus",
	}

	i.Desktops = []*tests.Desktop{
		drlmServer,
	}

	if serverOS == "debian8" || serverOS == "debian9" || serverOS == "ubuntu16.04" || serverOS == "ubuntu18.04" {
		requirements := "openssh-client openssl gawk nfs-kernel-server rpcbind isc-dhcp-server tftpd-hpa qemu-utils sqlite3 lsb-release bash-completion"
		i.PreCommands = []tests.Command{
			&tests.SSHCommand{
				CMD: fmt.Sprintf("apt update && apt upgrade -y && apt install -y %s", requirements),
			},
			&tests.SSHCommand{
				CMD: "apt install -y git build-essential debhelper golang",
			},
		}
		i.Commands = []tests.Command{
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  fmt.Sprintf("git clone %s", Repository),
			},
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  fmt.Sprintf("cd drlm && git checkout %s", Branch),
			},
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  "cd drlm && make deb",
			},
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  fmt.Sprintf("dpkg -i %s", debFile),
			},
		}
		i.PostCommands = []tests.Command{
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  `sed -i 's/GRUB_CMDLINE_LINUX="/GRUB_CMDLINE_LINUX="quiet max_loop=1024 /g' /etc/default/grub`,
			},
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  "grub2-mkconfig -o /boot/grub2/grub.cfg",
			},
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD: `echo 'service tftp
{
		socket_type = dgram
		protocol = udp
		wait = yes
		user = root
		server = /usr/sbin/in.tftpd
		server_args = -s /var/lib/drlm/store
		disable = no
		per_source = 11
		cps = 100 2
		flags = IPv4
}' > /etc/xinetd.d/tftp`,
			},
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  "systemctl restart xinetd.service",
			},
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  "systemctl enable xinetd.service",
			},
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  "systemctl restart rpcbind.service",
			},
			&tests.SSHCommand{
				Conn: drlmServer.Conn,
				CMD:  "systemctl enable rpcbind.service",
			},
		}
	}
}
