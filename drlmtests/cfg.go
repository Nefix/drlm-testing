/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package drlmtests

import (
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/isard"
)

// Repository is the Git URL of DRLM
const Repository string = "https://github.com/brainupdaters/drlm"

// Branch is the branch that is going to be used for the tests
const Branch string = "develop"

// ServerOS are all the different OS supported by DRLM as servers
var ServerOS = []string{
	// "debian8",
	"debian9",
	"ubuntu16.04",
	// "ubuntu18.04",
	// "centos6",
	"centos7",
	"leap42",
	"leap15",
}

// ClientOS are all the diferent OS supported by DRLM as clients
var ClientOS = []string{
	"debian7",
	"debian8",
	"debian9",
	"ubuntu12.04",
	"ubuntu14.04",
	"ubuntu16.04",
	"ubuntu18.04",
	"centos5",
	"centos6",
	"centos7",
	"leap42",
	"leap15",
}

// Templates are the templates that come with the Isard backup
var Templates = map[string]isard.Template{
	"debian7": isard.Template{
		ID:     "_admin_Template_Debian_7.11",
		Search: "Debian 7.11",
	},
	"debian8": isard.Template{
		ID:     "_admin_Template_Debian_8.11",
		Search: "Debian 8.11",
	},
	"debian9": isard.Template{
		ID:     "_admin_Template_Debian_9.8",
		Search: "Debian 9.8",
	},
	"ubuntu12.04": isard.Template{
		ID:     "_admin_Template_Ubuntu_12.04",
		Search: "Ubuntu 12.04",
	},
	"ubuntu14.04": isard.Template{
		ID:     "_admin_Template_Ubuntu_14.04",
		Search: "Ubuntu 14.04",
	},
	"ubuntu16.04": isard.Template{
		ID:     "_admin_Template_Ubuntu_16.04",
		Search: "Ubuntu 16.04",
	},
	"ubuntu18.04": isard.Template{
		ID:     "_admin_Template_Ubuntu_18.04",
		Search: "Ubuntu 18.04",
	},
	"centos5": isard.Template{
		ID:     "_admin_Template_CentOS_5.11",
		Search: "CentOS 5.11",
	},
	"centos6": isard.Template{
		ID:     "_admin_Template_CentOS_6.10",
		Search: "CentOS 6.10",
	},
	"centos7": isard.Template{
		ID:     "_admin_Template_CentOS_7.6",
		Search: "CentOS 7.6",
	},
	"leap42": isard.Template{
		ID:     "_admin_Template_openSUSE_Leap_42.3",
		Search: "openSUSE Leap 42.3",
	},
	"leap15": isard.Template{
		ID:     "_admin_Template_openSUSE_Leap_15.0",
		Search: "openSUSE Leap 15.0",
	},
}
