/*
 * Copyright (C) 2019 Néfix Estrada
 * Authors: Néfix Estrada
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"

	"gitea.nefixestrada.com/nefix/drlm-testing/drlmtests"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/docker"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/isard"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/libvirt"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/logger"
	"gitea.nefixestrada.com/nefix/drlm-testing/pkg/tests"

	log "github.com/sirupsen/logrus"
)

var i *isard.Isard

func cleanup() {
	if i != nil {
		log.Info("stopping Selenium")
		if err := i.Stop(); err != nil {
			log.WithError(err).Error("error stopping Selenium")
		}
	}

	log.WithField("command", "docker-compose down").Info("stopping IsardVDI")
	if err := docker.Down(); err != nil {
		log.WithError(err).Error("error stopping IsardVDI")
	}
}

func main() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		log.Warn("interrupting tests")
		cleanup()
		os.Exit(1)
	}()
	defer cleanup()

	setup := flag.Bool("setup", false, "execute the wizard and extract the templpates (the isard folder has to be empty)")
	flag.Parse()

	logger.Init()

	// Docker
	log.WithField("command", "docker-compose pull").Info("downloading latest IsardVDI version")
	if err := docker.Pull(); err != nil {
		log.WithError(err).Error("error downloading latest IsardVDI version")
		return
	}

	log.WithField("command", "docker-compose up").Info("starting IsardVDI")
	if err := docker.Up(); err != nil {
		log.WithError(err).Error("error starting IsardVDI")
		return
	}

	// Selenium initialization
	log.Info("starting Selenium")
	var err error
	i, err = isard.New("localhost", "pirineus")
	if err != nil {
		log.WithError(err).Error("error starting Selenium")
		return
	}

	// Isard Wizard
	if *setup {
		log.Info("executing the IsardVDI setup wizard")
		err = i.ExecuteWizard()
		if err != nil {
			log.WithError(err).Error("error executing the IsardVDI setup wizard")
			return
		}
	}

	// Isard Login
	log.Info("logging in to Isard")
	err = i.Login()
	if err != nil {
		log.WithError(err).Error("error logging in to Isard")
		return
	}

	// Isard Backup import
	if *setup {
		log.Info("importing templates backup")
		if err = i.ImportBackup("./drlm_testing.isard"); err != nil {
			log.WithError(err).Error("error importing templates backup")
			return
		}
	}

	// Connect to the Hypervisor
	log.Info("getting hypervisor IP address")
	ips, err := docker.GetContainerIPs("isard-hypervisor")
	if err != nil {
		log.WithError(err).Error("error getting the hypervisor IP")
		return
	}
	hyperIP := ips[0]

	log.Info("connecting to the hypervisor")
	i.Libvirt = &libvirt.Libvirt{
		Host: hyperIP,
	}
	if err = i.Libvirt.Connect(); err != nil {
		log.WithError(err).Error("error connecting with the hypervisor")
		return
	}

	// Prepare tests
	log.Info("preparing the tests")
	t := drlmtests.Prepare()

	// Run tests
	log.Info("running the tests")
	result := tests.Run(i, t)
	log.Info("finished running the tests")

	log.Info("saving the result")
	resultJSON, err := json.MarshalIndent(result, "", "\t")
	if err != nil {
		log.WithError(err).Error("error encoding the result to JSON")
		log.Info(result)
		return
	}

	if err = ioutil.WriteFile("result.json", resultJSON, 0644); err != nil {
		log.WithError(err).Error("writting the result to the result file")
		log.Info(result)
		return
	}

	// if err = i.CreateDesktop(isard.Desktop{
	// 	Name: "drlm-server-5",
	// 	Template: isard.Template{
	// 		Search: "Debian 9.8",
	// 		ID:     "_admin_Template_Debian_9.8",
	// 	},
	// 	Hardware: isard.Hardware{
	// 		CPUs: 1,
	// 		RAM:  500,
	// 	},
	// }); err != nil {
	// 	log.WithError(err).Error("error creating the desktop")
	// 	return
	// }

	// l := libvirt.Libvirt{"172.25.0.3"}
	// if err := l.Connect(); err != nil {
	// 	log.Fatal(err)
	// }

	// serverIP, err := l.GetDomainIP("_admin_drlm-server")
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// sshConfig := &ssh.ClientConfig{
	// 	User: "root",
	// 	Auth: []ssh.AuthMethod{
	// 		ssh.Password("isard"),
	// 	},
	// 	HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	// }

	// remoteSSHConfig := &ssh.ClientConfig{
	// 	User: "root",
	// 	Auth: []ssh.AuthMethod{
	// 		ssh.Password("pirineus"),
	// 	},
	// 	HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	// }

	// local := &customSSH.Endpoint{
	// 	Host: "localhost",
	// 	Port: 9988,
	// }

	// server := &customSSH.Endpoint{
	// 	Host: "172.25.0.3",
	// 	Port: 22,
	// }

	// remote := &customSSH.Endpoint{
	// 	Host: "192.168.122.170",
	// 	Port: 22,
	// }

	// tun := &customSSH.Tunnel{
	// 	Local:  local,
	// 	Server: server,
	// 	Remote: remote,
	// 	Config: sshConfig,
	// }

	// go func() {
	// 	if err := tun.Start(); err != nil {
	// 		log.Fatalf("error starting the SSH tunnel: %v", err)
	// 	}
	// }()

	// sshConn := customSSH.SSH{
	// 	Host: "localhost",
	// 	Port: 9988,
	// }
	// if err := sshConn.Connect(remoteSSHConfig); err != nil {
	// 	log.Fatalf("error connecting to the hypervisor: %v", err)
	// }

	// out, err := sshConn.Run("hostname")
	// if err != nil {
	// 	log.Fatalf("error getting the hostname: %v: %s", err, out)
	// } else {
	// 	log.Println(out)
	// }

	// log.Println(sshConn.RunWithStdin("passwd", 1, "test", "test"))
}
