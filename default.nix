{
  pkgs ? import <nixpkgs> {},
  goPath ? "go_1_12"
}:
  with pkgs;
  let
    go = lib.getAttrFromPath (lib.splitString "." goPath) pkgs;
    buildGoPackage = pkgs.buildGoPackage.override { inherit go; };
    dep2src = goDep:
      {
        inherit (goDep) goPackagePath;
        src = if goDep.fetch.type == "git" then
          fetchgit {
            inherit (goDep.fetch) url rev sha256;
          }
        else if goDep.fetch.type == "hg" then
          fetchhg {
            inherit (goDep.fetch) url rev sha256;
          }
        else if goDep.fetch.type == "bzr" then
          fetchbzr {
            inherit (goDep.fetch) url rev sha256;
          }
        else if goDep.fetch.type == "FromGitHub" then
          fetchFromGitHub {
            inherit (goDep.fetch) owner repo rev sha256;
          }
        else abort "Unrecognized package fetch type: ${goDep.fetch.type}";
      };
  in
    buildGoPackage {
      name = "drlm-testing";
      version = "1.0.0";
      goPackagePath = "gitea.nefixestrada.com/nefix/drlm-testing";
      src = lib.cleanSourceWith {
        filter = (path: type:
          ! (builtins.any
            (r: (builtins.match r (builtins.baseNameOf path)) != null)
            [
              ".env"
              ".go"
              "drlm_testing.isard"
            ])
        );
        src = lib.cleanSource ./.;
      };
      goDeps = ./deps.nix;
      propagatedBuildInputs = with pkgs; [ pkg-config libvirt ];
    }
